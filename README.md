# Crostini Archiver:

Worry no more with Crostini system instability!

Crostini is beta software and as such is highly prone to malfunction and data loss. This software allows you to run a script in the developer mode command shell and automatically backup the entire user home folder. The software allows you to choose the desired drive and will automatically archive you last save to a compressed file in case any of the data in the home directory is unreadable or an older version needs to be accessed. The program is designed for small home folders under 1 GB but will work on larger ones so long as the output drive has double the capacity and you have patience. This program does not require any third-party tools to be installed to run.

### _Notice_
_This software is essentially abandomware at this point. I may periodically publish bug fixes and quality of life improvements, but this program is outdated and was poorly written in the first place. If, some day, demand for this software increases, I may reconsider. However, Crostini has come a long way since this software was created, and it is very unlikely that people will ever benefit from any additions of features or bug fixes. That said, I believe in creating high quality, stable products that people can reasonably expect to work. I also value hight quality code that people can learn from. Therefore, I will make any changes I deem necessary to improve security and stability for anyone who may banefit from it._

## How to use:

Prerequisites: [Developer mode enabled](https://www.chromium.org/chromium-os/poking-around-your-chrome-os-device). Crostini container installed and running.
Note: The following commands DO NOT require root privileges:

1.  Download the script from the master branch. It is called "crostini-archiver.sh"
2.  Press <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>T</kbd>` on the keyboard to open the Crosh terminal emulator.
3.  type `shell` and hit <kbd>Enter</kbd>/<kbd>Return</kbd> on your keyboard.
4.  `cd` to the folder that you put it in. For this example it's in `~/Downloads`
5.  After running `cd ~/Downloads`, type `bash crostini-archiver` and hit <kbd>Enter</kbd>/<kbd>Return</kbd>.
6.  Type the name of your drive of choice and hit <kbd>Enter</kbd>/<kbd>Return</kbd>!

## Decoding error messages:

Sometimes you may encounter an error. Our error codes are formatted `Err: XX-XXXXX`

*  `Err: CA-DRVEM` Means that there is a problem with the drive path. If you did not tell the archiver where to store the archive it will give you this error. Make sure you type the drive name into the text entry.
*  `Err: CA-DRVNA` Means that the drive name you've entered is not valid. It may have been ejected or it may have had case/punctuation errors. Make sure the drive name is entered exactly as seen.
*  `Err: CA-PREL` Means that there is a problem with permissions. This is commonly related to running as root. If you get this error make sure you're not running it as root or with `sudo`.

## Tips and tricks:

Crostini Archiver has some extra features designed to make workflows faster for experienced users!

1.  If you already know the name of the drive you want to make the archive to simply type in the command and then type the name of the drive after it. Example: `bash ./crostini-archiver SD\ Card`
2.  The script is heavily commented. Editing the script will allow you to make modifications and easily tell how things work in case you need it to work differently!
3.  If a file or all files in a new archive are deleted you can extract the previous archive with `tar -xvf /media/removable/$drive/crostini-archiver/backup-archive.tar` and replace the corrupt/lost files! (Make sure you don't have a file named `backup-archive.tar` in your Crostini home folder or it will overwrite this feature!)
4.  If you type the command followed by `--help` or `-h` you will see basic program help.
5.  If you want to purge the archive folder simply type the command followed by the `--purge` or `-p` flag.
6.  If you want to backup the current archive without rerunning the entire command simply enter the command followed by the `--backup` or the `-b` flags.
7.  Appending the `--no-confirm` flag will allow the script to run completely independent of user input. (WIP. May require some user input still.)

## Note
_Anything labeled "TODO" or "WIP" will, most likely, never be added. This program is mostly abandomware._
