#!/bin/bash
## Licensed freely under MIT. Check 'LICENSE' for details.
## Tool developed by TriVoxel (Caden R. Mitchell)
## Intended as simple crosh script to automatically archive the
## unstable crostini filesystem to a reliable SD card/USB key
## to prevent data loss from container issues and instability.
## Recommended for home folders under 2 GB. Make sure output
## drive has double or more space than container's home folder
## to prevent drive malfunction/corruption.
## Designed for Chrome OS Crosh shell. DEVELOPER MODE REQUIRED!
## https://TriVoxel.page.link/cros-arch

# Check if program has root privileges.
if [[ "$EUID" = "0" ]]; then
	echo "\
This program should not be run as root
Err: CA-PREL
Report to https://gitlab.com/TriVoxel/crostini-archiver/issues
Exiting."
	exit
fi

echo "\
riVoxel Software
Created by TriVoxel (Caden Mitchell)"
case "$1" in
	"-h"|"--help"|"help") echo -e "\n" ;;
	*) echo -e "You can use 'Ctrl+C' at any time to cancel...\n"; sleep .5s ;;
esac

## Shell options
# Enable/disable reversing true/false values
.ext () {
	case "$1" in
		"-s") shopt -s extglob ;;
		"-u") shopt -u extglob ;;
	esac
}

# Enable/disable removing case sensitivity for pols
.case () {
	case "$1" in
		"-s") shopt -s nocasematch ;;
		"-u") shopt -u nocasematch ;;
	esac
}
# This will require unsetting all shell options on any exit.

.ext -s
# Workaround for detecting flags as drives.
if [[ "$1" != "--no-confirm" && "$1" != "--purge" && "$1" != "-b" && "$1" != "--backup" ]];then
	drive="$1"
elif [[ "$2" != "--no-confirm" && "$2" != "--purge" && "$2" != "-b" && "$2" != "--backup" ]];then drive="$2"
else drive="$3"
fi

# TODO: Allow selecting input source manually.
.case -u
inpath="/media/fuse/*/*"
outpath="/media/removable/$drive/crostini-archiver"
.case -s

if [[ "$1" = "--help" || "$1" = "-h" || "$1" = "-H" ]];then
	echo "Help for Crostini Archiver"
	echo "Usage: crostini-archiver [DRIVE]... OPTION..."
	echo -e "Automatically archive the Crostini user directory to a removable medium.\n"
	echo -e "With no DRIVE or OPTION, Crostini Archiver will run normally.\n"
	echo "Options list:"
	echo "  -b, --backup        Backs up the selected drive then exits"
	echo "  --no-confirm        Doesn't ask for confirmation on operations"
	echo "  -p, --purge         Delete all data on selected drive"
#	echo "  -v, --version       Display program version and release date"
	echo "  -h, --help          Display this help dialogue"
	echo -e "\nMore info at https://TriVoxel.page.link/cros-arch"
	exit
fi

if [[ -z "$drive" ]];then
	echo "Welcome to Crostini Archiver!"
	sleep 1s
	echo -e "\n<====================>"
	echo Drive list:
	# List drives for user:
	# TODO: Remove unnecessary symbols (. and ..)
	ls -a /media/removable/
	echo -e "\nEnter desired drive below to begin archive:\n"
	# Once drive name is given we should be clear to finish operation.
	read -r "drive"; echo -e "\n"
	echo "\
Welcome to Crostini Archiver!

<=============================>
Drive list:
$(ls -a /media/removable)

Enter desired drive below to begin archive:"
	read -r "drive"
fi

# Set output drive path if not already defined
outpath="/media/removable/$drive/crostini-archiver"

#if [ -d $outpath/ ];then
if [[ "$1" = "-p" || "$1" = "--purge" || "$2" = "--purge" || "$2" = "-p" ]];then
	echo "This will DESTROY all files in the 'crostini-archive' folder of '$drive'!"
	echo "Continue? (Y/n)"
	if [[ "$1" = "--no-confirm" || "$2" = "--no=confirm" || "$3" = "--no-confirm" ]];then
		confirm="Y"
	else
		.case -s; read -r "confirm"
	fi
	if [[ "$confirm" = "Y" || "$1" = "--no-confirm" || "$2" = "--no-confirm" || "$3" = "--no-confirm" ]];then
		.case -u
		echo "Removing all files from drive's archive folder..."
		sleep 0.5
		rm -rvf "$outpath/!($outpath/backup-archive.tar)"
		echo -e "\nAll files in '$outpath/' removed."
		echo "Exiting."
		exit 0
	else
		echo -e "\nOperation manually cancelled."
		echo "No files changed!"
		echo "Exiting."
		exit 0
	fi
elif [[ "$1" = "-b" || "$1" = "--backup" || "$2" = "-b" || "$2" = "--backup" || "$3" = "-b" || "$3" = "--backup" ]];then
	echo "Backing up all files currently in '$outpath/' folder."
	sleep 1s
	if [ -d "$outpath/backup-archive.tar" ];then
		rm -rvf "$outpath/backup-archive.tar"
	fi
	tar -cvf "$outpath/backup-archive.tar" "$outpath/*"
	echo "Operation finished successfully.";echo "Exiting."
	exit 0
fi
#fi

echo "Attempting to create archive in 'crostini-archiver' directory in drive:'$drive'; please wait..."
sleep 1s
echo -e "\n"

# Check if a drive has been specified. If not force exit.
if [[ -z "$drive" ]]; then
	.ext -u
	echo "No device specified. Run \`ls -a /media/removable/\` to view all possible output drives."
	echo "Err: CA-DRVEM"
	echo "Report to https://gitlab.com/TriVoxel/crostini-archiver/issues"
	echo "Exiting..."
	exit
fi

# In case user inputs incorrect drive name or drive is unmounted during text entry check for existance once more:
if [[ ! -d "/media/removable/$drive/" ]]; then
	.ext -u
	echo "\
Drive does not exist or is no longer operational.
Was '$drive' spelled correctly?
Try running \`ls -a /media/removable/\` for manual list of drives.
Err: CA-DRVNA
Report to https://gitlab.com/TriVoxel/crostini-archiver/issues
Exiting..."
	exit
fi

# Ask user if they're sure they want to continue in case they change their mind.
if [[ "$1" != "--no-confirm" && "$2" != "--no-confirm" && "$3" != "--no-confirm" ]];then
	echo "Are you sure you want to continue with this operation?  ""$drive"" must have at least twice as much free space as the total size of your home folder! (Y/n)"
	read -r "confirm"
	.case -s
	if [[ ! "$confirm" = "Y" ]];then
		.case -u
		.ext -u
		echo -e "Operation aborted. No files changed.\nExiting."
		exit
	fi
fi

if [[ ! -d "$outpath/" ]]; then
	echo "Backup folder not found in '$drive', attempting to create..."
	sleep .5
	mkdir "$outpath/"
	echo "Backup folder created in ""$outpath"
	sleep .5
fi

# Move old files in archive folder to a compressed file in case newly copied files are corrupt:
if [[ "$(ls -A "$outpath/")" ]];then
	echo "Moving old Archive to compressed file, please wait...";sleep 1s
	if [[ -d "$outpath/backup-archive.tar" ]];then
		tar -cvf "$outpath/backup-archive.tar" "$outpath/!($outpath/backup-archive.tar)"
	else
		tar -cvf "$outpath/backup-archive.tar" "$outpath/*"
	fi
	echo "Backup archived. Check '$outpath/backup-archive.tar' to access previous archive."
	sleep 1
else
	echo "No files to backup. Continuing..."
	sleep 1
fi

if [[ "$(ls -A "$outpath/")" ]];then
echo "Would you like to continue deleting all files in '$outpath'? (Y/n)"
	if [[ "$1" = "--no-confirm" || "$2" = "--no=confirm" || "$3" = "--no-confirm" ]];then
		purge="Y"
	else
		read -r "purge"
	fi
else
	echo "'$outpath/' is empty. Skipping purge."
fi

.case -s
if [[ "$purge" = "Y" && "$(ls -A "$outpath/")" ]];then
	.case -u
	if [[ "$1" != "--no-confirm" && "$2" != "--no-confirm" && "$3" != "--no-confirm" ]];then
		echo "To prevent this message in the future, launch with the '--no-confirm' flag."
		sleep 2s
	fi
	echo -e "\e[31mIn 4 seconds, file purge will begin..."
	sleep 4s
	echo -e "\e[31mBeginning..."
	sleep 1s
elif [[ "$(ls -A "$outpath/")" ]];then
	echo -e "\e[0mPurge cancelled."
	echo "Exiting."
	.case -u
	.ext -u
	exit 0
fi

# Clear existing folders to prevent unused files and directories from building up.
if [[ -d "$outpath/backup-archive.tar" ]];then
	rm -rvf "${outpath/:?}backup-archive.tar"
else
	rm -rvf "${outpath/:?}*"
fi

# Copy entire home directory of $source to $drive:
echo -e "\e[0mCopying full crostini home folder to drive."
echo "This process can take a long time, please be patient..."
sleep 1s
echo "Starting..."
sleep 0.5s

# Copy Crostini folder into removable medium but skip over 'backup-archive.tar' to prevent overwriting backup.
if [ -d "$inpath/backup-archiver.tar" ];then
	cp -rvf "$inpath" "$outpath""/!(""$inpath""/backup-archive.tar)"
else
	cp -rvf "$inpath" "$outpath"
fi

sleep 1s
echo -e "\n<==============================>"
if [[ $(ls -A "$outpath/") ]];then
	echo "\
Script executed successfully. No issues detected!
Operation complete. Files should be fine but check for corrupted important files. Previous archive placed in '$outpath/backup-archive.tar' and can be extracted if there are errors.
More features planned. Check '#TODO' and '#INPROGRESS' tags in script for details.
If you have encountered an issue, please post to https://gitlab.com/TriVoxel/crostini-archiver/issues"
else
	echo "\
Ouput files detected as non-existent. No directories found in '$outpath'. Check manually with 'ls -a $outpath/*'.
Err: CA-OUTNA
Report to https://gitlab.com/TriVoxel/crostini-archiver/issues.
Exiting..."
	.ext -u
	.case -u
	exit 1
fi

# TODO: Allow archiver to work without Dev Mode enabled. Should run in Crostini independently. Requires USB device support

### Final notes from developer ###
# This software is essentially abandomware at this point. I may periodically publish bug fixes and quality of life improvements,
# but this program is outdated and was poorly written in the first place. If there is one day a demand for this software, I may
# reconsider. However, Crostini has come a long way since this software was created and it is very unlikely that people will ever
# benefit from any additions of features or bug fixes. That said, I believe in creating high quality, stable products that people
# can use. Therefore, I will make any changes I deem necessary to improve security and stability for anyone who may need it.
